---
title: "Paderborn University"
logo: upb.png
link: http://www.uni-paderborn.de/
weight: 1
---

The university allows us to use their rooms and provides us with resources like virtual machines.
