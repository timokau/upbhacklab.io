#!/bin/sh
set -xe
if [[ $(find static/avatars/ -name "*.jpg" -size +50k) ]]
then
    >&2 echo "There are avatar files which are larger than 50kb!"
    >&2 echo "Use \`convert <orig> -resize <number>% <output>\` to scale down!"
    exit 1
fi